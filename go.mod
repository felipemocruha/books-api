module gitlab.com/felipemocruha/books-api

go 1.15

require (
	github.com/dgraph-io/badger/v2 v2.2007.2
	github.com/labstack/echo/v4 v4.1.17
	github.com/rs/zerolog v1.20.0
)
