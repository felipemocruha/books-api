package main

import (
	"encoding/json"
	"github.com/rs/zerolog/log"
	
	"github.com/labstack/echo/v4"
	badger "github.com/dgraph-io/badger/v2"
)

type Book struct {
	ISBN string `json:"isbn"`
	Title string `json:"title"`
	Pages int `json:"pages"`
}

type Service struct {
	db *badger.DB
}

func (s *Service) CreateBook(ctx echo.Context) error {
	book := &Book{}
	if err := ctx.Bind(book); err != nil {
		log.Error().Msgf("%v",err)
		return err
	}

	err := s.db.Update(func(txn *badger.Txn) error {
		payload, err := json.Marshal(book)
		if err != nil {
			log.Error().Msgf("%v",err)
			return err
		}

		return txn.Set([]byte(book.ISBN), payload)
	})
	if err != nil {
		log.Error().Msgf("%v",err)
		return err
	}

	return ctx.JSON(201, map[string]string{"message": "book created!"})
}

func (s *Service) BookDetails(ctx echo.Context) error {
	isbn := ctx.Param("isbn")
	bookBuf := []byte{}
	
	err := s.db.View(func(txn *badger.Txn) error {
		item, err := txn.Get([]byte(isbn))
		if err != nil {
			log.Error().Msgf("%v",err)
			return err
		}

		return item.Value(func(val []byte) error {
			bookBuf = append(bookBuf, val...)
			return nil
		})
	})
	if err != nil {
		log.Error().Msgf("%v",err)
		return err
	}

	book := &Book{}
	if err := json.Unmarshal(bookBuf, book); err != nil {
		log.Error().Msgf("%v",err)
		return err
	}

	return ctx.JSON(200, book)
}

func (s *Service) DeleteBook(ctx echo.Context) error {
	isbn := ctx.Param("isbn")
	
	err := s.db.Update(func(txn *badger.Txn) error {
		return txn.Delete([]byte(isbn))
	})
	if err != nil {
		log.Error().Msgf("%v",err)
		return err
	}

	return nil
}

func (s *Service) ListBooks(ctx echo.Context) error {
	books := []Book{}

	err := s.db.View(func(txn *badger.Txn) error {
		opts := badger.DefaultIteratorOptions
		it := txn.NewIterator(opts)
		defer it.Close()
		
		for it.Rewind(); it.Valid(); it.Next() {
			err := it.Item().Value(func(v []byte) error {
				book := &Book{}
				if err := json.Unmarshal(v, book); err != nil {
					log.Error().Msgf("%v",err)
					return err
				}
				books = append(books, *book)

				return nil
			})
			if err != nil {
				log.Error().Msgf("%v",err)
				return err
			}
		}
		
		return nil
	})
	if err != nil {
		log.Error().Msgf("%v", err)
		return err
	}

	return ctx.JSON(200, books)	
}

func createRoutes(e *echo.Echo, s *Service) {
	e.GET("/books", s.ListBooks)
	e.POST("/books", s.CreateBook)
	e.GET("/books/:isbn", s.BookDetails)
	e.DELETE("/books/:isbn", s.DeleteBook)
}

func main() {
	db, err := badger.Open(badger.DefaultOptions("database"))
	if err != nil {
		log.Fatal().Msg(err.Error())
	}
	defer db.Close()

	server := echo.New()
	createRoutes(server, &Service{db})

	server.Logger.Fatal(server.Start("0.0.0.0:8000"))
}
